# README #

This is a controller for the scoreboard marquee made https://bitbucket.org/theismanj/scoreboard-server and https://bitbucket.org/theismanj/nhl-marquee. This updates settings on the server which then will display on the marquee. Screenshots of the application can be found below.

![Screenshot_20170312-162251.png](https://bitbucket.org/repo/akby5Rk/images/566194956-Screenshot_20170312-162251.png)
![Screenshot_20170312-162312.png](https://bitbucket.org/repo/akby5Rk/images/3084332890-Screenshot_20170312-162312.png)
![Screenshot_20170312-162319.png](https://bitbucket.org/repo/akby5Rk/images/4229563101-Screenshot_20170312-162319.png)
![Screenshot_20170312-162327.png](https://bitbucket.org/repo/akby5Rk/images/138662937-Screenshot_20170312-162327.png)

### How do I get set up? ###

The application should be all ready to run once cloned; however, a properties file will need to be placed at app/src/main/assets/config.properties. The properties will need to be "service.url" and "auth.header". "service.url" should be the URL to hit the service from https://bitbucket.org/theismanj/scoreboard-server and the "auth.header" property should be the username and password setup in that same project.