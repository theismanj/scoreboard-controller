package nhl.scoreboard.scoreboardcontroller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.scoreboard.pojo.AvailableStats;

import nhl.scoreboard.scoreboardcontroller.listener.GameSelectedListener;

public class GamesAndStatsActivity extends BaseActivity implements GameSelectedListener {
    private boolean firstClick = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_games_and_stats);
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onGameSelected(AvailableStats stats) {
        StatsFragment statsFragment = (StatsFragment)getFragmentManager()
                .findFragmentById(R.id.statsFragment);

        if (firstClick && statsFragment.getView() != null) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    0, LinearLayout.LayoutParams.MATCH_PARENT, 1f);
            statsFragment.getView().setLayoutParams(layoutParams);
            firstClick = false;
        }

        statsFragment.updateGameSelection(stats);
    }
}
