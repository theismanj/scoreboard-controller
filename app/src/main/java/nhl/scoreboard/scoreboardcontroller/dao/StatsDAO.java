package nhl.scoreboard.scoreboardcontroller.dao;

import android.annotation.SuppressLint;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.scoreboard.pojo.AvailableStats;
import com.scoreboard.pojo.Mode;
import com.scoreboard.pojo.Settings;
import com.scoreboard.pojo.StatSetting;
import com.scoreboard.pojo.nhl.StatType;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nhl.scoreboard.scoreboardcontroller.cache.StatsCache;
import nhl.scoreboard.scoreboardcontroller.util.PropertiesUtil;

public class StatsDAO {
    private static final int TIMEOUT = 30000;
    private static final String GET = "GET";
    private static final String POST = "POST";

    public static List<AvailableStats> getAvailableStats() {
        try {
            HttpURLConnection connection = getConnection("games/statTypes/1", GET);

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder json = new StringBuilder();
            String temp;
            while ((temp = reader.readLine()) != null) {
                json.append(temp);
            }
            reader.close();

            Gson gson = new Gson();
            List<AvailableStats> availableStatses = gson.fromJson(json.toString(), new TypeToken<List<AvailableStats>>(){}.getType());
            Collections.sort(availableStatses, new Comparator<AvailableStats>() {
                @Override
                public int compare(AvailableStats a1, AvailableStats a2) {
                    return a1.getGame().getId() - a2.getGame().getId();
                }
            });

            return availableStatses;
        } catch (IOException e) {
            Log.println(Log.ERROR, "StatsDAO", "Error Fetching games!\n" + Log.getStackTraceString(e));
            return null;
        }
    }

    public static void saveStatsSelections() {
        try {
            HttpURLConnection conn = getConnection("settings/1", POST);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));

            writer.write(new Gson().toJson(generateSettings()));

            writer.flush();
            writer.close();
            os.close();
            int responseCode=conn.getResponseCode();

            if (responseCode != 204) {
                throw new RuntimeException("Unexpected error saving settings");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static HttpURLConnection getConnection(String serviceUrl, String requestMethod) throws IOException {
        URL url = new URL(PropertiesUtil.getServiceURL() + serviceUrl);

        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        connection.setReadTimeout(TIMEOUT);
        connection.setConnectTimeout(TIMEOUT);
        connection.setRequestMethod(requestMethod);
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setRequestProperty("Authorization", PropertiesUtil.getAuthorizationHeader());

        return connection;
    }

    private static Settings generateSettings() {
        StatsCache statsCache = StatsCache.getInstance();
        @SuppressLint("UseSparseArrays") Map<Integer, Set<StatType>> gameIdToStatsSelected = new HashMap<>();

        for (AvailableStats availableStats : statsCache.get()) {
            Set<StatType> statTypesForGame = new HashSet<>();

            for (StatSetting statSetting : availableStats.getStatSettings()) {
                if (statSetting.isEnabled()) {
                    statTypesForGame.add(statSetting.getStatType());
                }
            }

            gameIdToStatsSelected.put(availableStats.getGame().getId(), statTypesForGame);
        }

        return new Settings(Mode.NHL_SCORES, gameIdToStatsSelected);
    }
}
