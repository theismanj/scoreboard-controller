package nhl.scoreboard.scoreboardcontroller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.scoreboard.pojo.AvailableStats;
import com.scoreboard.pojo.StatSetting;
import com.scoreboard.pojo.nhl.Game;

import java.util.ArrayList;

import nhl.scoreboard.scoreboardcontroller.R;
import nhl.scoreboard.scoreboardcontroller.cache.StatsCache;

public class GameAdapter extends ArrayAdapter<AvailableStats> {
    private static final StatsCache statsCache = StatsCache.getInstance();

    public GameAdapter(Context context, ArrayList<AvailableStats> availableStatses) {
        super(context, 0, availableStatses);
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // Get the data item for this position
        final AvailableStats availableStats = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_game_information, parent, false);
        }

        TextView gameText = (TextView) convertView.findViewById(R.id.gameText);
        TextView periodText = (TextView) convertView.findViewById(R.id.periodText);
        final Switch showHideSwitch = (Switch) convertView.findViewById(R.id.showHideSwitch);

        if (availableStats == null) {
            gameText.setText(R.string.error);
        } else {
            final Game game = availableStats.getGame();
            StringBuilder text = new StringBuilder();
            text.append(game.getTeams().getAwayTeam().getScore())
                    .append("-")
                    .append(game.getTeams().getAwayTeam().getTeam().getAbbreviation());

            text.append(" @ ");

            text.append(game.getTeams().getHomeTeam().getScore())
                    .append("-")
                    .append(game.getTeams().getHomeTeam().getTeam().getAbbreviation());

            gameText.setText(text.toString());
            if (game.getStatus().getDetailedState().equalsIgnoreCase("Final")) {
                periodText.setText(R.string.final_text);
            } else {
                periodText.setText(game.getLinescore().getCurrentPeriodOrdinal());
            }

            boolean checkSwitch = false;
            for (StatSetting statSetting : availableStats.getStatSettings()) {
                if (statSetting.isEnabled()) {
                    checkSwitch = true;
                    break;
                }
            }

            showHideSwitch.setChecked(checkSwitch);

            showHideSwitch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (showHideSwitch.isChecked()) {
                        statsCache.setDefaultStatsForGame(game);
                    } else {
                        statsCache.clearStatsForGame(game);
                    }
                }
            });
        }

        return convertView;
    }


}
