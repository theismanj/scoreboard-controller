package nhl.scoreboard.scoreboardcontroller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.scoreboard.pojo.StatSetting;
import com.scoreboard.pojo.nhl.Game;

import java.util.ArrayList;

import nhl.scoreboard.scoreboardcontroller.R;
import nhl.scoreboard.scoreboardcontroller.cache.StatsCache;

public class StatsAdapter extends ArrayAdapter<StatSetting> {
    private static final StatsCache statsCache = StatsCache.getInstance();
    private Game game;

    public StatsAdapter(Context context, ArrayList<StatSetting> statSettings) {
        super(context, 0, statSettings);
        this.setNotifyOnChange(true);
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        // Get the data item for this position
        final StatSetting statSetting = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_stat_setting, parent, false);
        }

        TextView statName = (TextView) convertView.findViewById(R.id.statName);
        final Switch showHideSwitch = (Switch) convertView.findViewById(R.id.showHideSwitch);

        if (statSetting == null) {
            statName.setText(R.string.error);
        } else {
            statName.setText(statSetting.getStatType().getDisplayValue());

            showHideSwitch.setChecked(statSetting.isEnabled());

            showHideSwitch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (game != null) {
                        statsCache.setStatForGame(game, statSetting.getStatType(), showHideSwitch.isChecked());
                    }
                }
            });
        }

        return convertView;
    }
}
