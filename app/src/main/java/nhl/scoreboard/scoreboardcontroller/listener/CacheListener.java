package nhl.scoreboard.scoreboardcontroller.listener;

public interface CacheListener {
    void onCacheUpdated();
}
