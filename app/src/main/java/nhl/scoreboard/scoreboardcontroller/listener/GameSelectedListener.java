package nhl.scoreboard.scoreboardcontroller.listener;

import com.scoreboard.pojo.AvailableStats;

public interface GameSelectedListener {
    void onGameSelected(AvailableStats stats);
}
