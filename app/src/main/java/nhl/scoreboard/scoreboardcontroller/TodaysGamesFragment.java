package nhl.scoreboard.scoreboardcontroller;

import android.app.Activity;
import android.app.ListFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.scoreboard.pojo.AvailableStats;

import java.util.ArrayList;
import java.util.List;

import nhl.scoreboard.scoreboardcontroller.adapter.GameAdapter;
import nhl.scoreboard.scoreboardcontroller.cache.StatsCache;
import nhl.scoreboard.scoreboardcontroller.dao.StatsDAO;
import nhl.scoreboard.scoreboardcontroller.listener.CacheListener;
import nhl.scoreboard.scoreboardcontroller.listener.GameSelectedListener;

/**
 * A placeholder fragment containing a simple view.
 */
public class TodaysGamesFragment extends ListFragment implements CacheListener {

    private static final StatsCache statsCache = StatsCache.getInstance();

    private ArrayAdapter<AvailableStats> gamesAdapter;
    private ArrayList<AvailableStats> availableStats = new ArrayList<>();
    private GameSelectedListener listener;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        gamesAdapter = new GameAdapter(getActivity(), availableStats);
        setListAdapter(gamesAdapter);

        try {
            listener = (GameSelectedListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(getActivity().toString()
                    + " must implement GameSelectedListener");
        }

        statsCache.subscribeToUpdateEvents(this);

        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        if (statsCache.get().isEmpty()) {
            Activity activity = getActivity();

            if (activity instanceof BaseActivity) {
                ((BaseActivity) activity).fetchStatsAndCache();
            }
        } else {
            updateAdapter();
        }
        super.onResume();
    }

    @Override
    public void onCacheUpdated() {
        updateAdapter();
    }

    private void updateAdapter() {
        List<AvailableStats> availableStatsList = statsCache.get();

        if (availableStatsList != null) {
            availableStats.clear();
            availableStats.addAll(availableStatsList);
        }

        if (gamesAdapter != null) {
            gamesAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        listener.onGameSelected((AvailableStats)l.getAdapter().getItem(position));
    }
}
