package nhl.scoreboard.scoreboardcontroller.cache;

public interface Cache<T> {
    void set(T object);

    T get();

    void clearCache();
}
