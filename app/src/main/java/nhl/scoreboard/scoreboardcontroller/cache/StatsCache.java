package nhl.scoreboard.scoreboardcontroller.cache;

import com.scoreboard.pojo.AvailableStats;
import com.scoreboard.pojo.StatSetting;
import com.scoreboard.pojo.nhl.Game;
import com.scoreboard.pojo.nhl.StatType;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import nhl.scoreboard.scoreboardcontroller.listener.CacheListener;

public class StatsCache implements Cache<List<AvailableStats>> {
    private static final StatsCache INSTANCE = new StatsCache();

    private final Set<CacheListener> listeners = new HashSet<>();
    private List<AvailableStats> availableStatsList = new ArrayList<>();

    private StatsCache() {
    }

    public static StatsCache getInstance() {
        return INSTANCE;
    }


    public void setStatForGame(Game game, StatType statType, boolean isSelected) {
        for (AvailableStats availableStats : availableStatsList) {
            if (availableStats.getGame().getId().equals(game.getId())) {
                for (StatSetting statSetting : availableStats.getStatSettings()) {
                    if (statSetting.getStatType().equals(statType)) {
                        statSetting.setEnabled(isSelected);
                        break;
                    }
                }
                break;
            }
        }
        fireUpdateEvents();
    }

    public void clearStatsForGame(Game game) {
        for (AvailableStats availableStats : availableStatsList) {
            if (availableStats.getGame().getId().equals(game.getId())) {
                clearStats(availableStats);
            }
        }
        fireUpdateEvents();
    }

    public void setDefaultStatsForGame(Game game) {
        for (AvailableStats availableStats : availableStatsList) {
            if (availableStats.getGame().getId().equals(game.getId())) {
                setDefaultStats(availableStats);
            }
        }
        fireUpdateEvents();
    }

    public void subscribeToUpdateEvents(CacheListener listener) {
        listeners.add(listener);
    }

    public void unsubscribeToUpdateEvents(CacheListener listener) {
        listeners.remove(listener);
    }

    private void clearStats(AvailableStats availableStats) {
        for (StatSetting statSetting : availableStats.getStatSettings()) {
            statSetting.setEnabled(false);
        }
    }

    private void setDefaultStats(AvailableStats availableStats) {
        for (StatSetting statSetting : availableStats.getStatSettings()) {
            switch (statSetting.getStatType()) {
                case GOALS:
                case SHOTS:
                    statSetting.setEnabled(true);
            }
        }
    }

    private void fireUpdateEvents() {
        for (CacheListener listener : listeners) {
            listener.onCacheUpdated();
        }
    }

    @Override
    public void set(List<AvailableStats> availableStatsList) {
        this.availableStatsList = availableStatsList;
        fireUpdateEvents();
    }

    @Override
    public List<AvailableStats> get() {
        return availableStatsList;
    }

    @Override
    public void clearCache() {
        availableStatsList.clear();
    }
}
