package nhl.scoreboard.scoreboardcontroller.pojo;

public class GameInformation {
    private int id;
    private String awayTeam;
    private String homeTeam;

    public GameInformation(int id, String awayTeam, String homeTeam) {
        this.awayTeam = awayTeam;
        this.homeTeam = homeTeam;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(String awayTeam) {
        this.awayTeam = awayTeam;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    @Override
    public String toString() {
        return awayTeam + " @ " + homeTeam;
    }
}
