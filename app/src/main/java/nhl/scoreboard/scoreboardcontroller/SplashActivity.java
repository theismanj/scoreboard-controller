package nhl.scoreboard.scoreboardcontroller;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import nhl.scoreboard.scoreboardcontroller.util.PropertiesUtil;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        PropertiesUtil.initProperties(getBaseContext());

        super.onCreate(savedInstanceState);

        Intent intent;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            intent = new Intent(this, GamesAndStatsActivity.class);
        } else {
            intent = new Intent(this, TodaysGamesActivity.class);
        }

        startActivity(intent);
        finish();
    }
}