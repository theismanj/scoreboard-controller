package nhl.scoreboard.scoreboardcontroller;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;

import com.scoreboard.pojo.AvailableStats;

import nhl.scoreboard.scoreboardcontroller.listener.GameSelectedListener;

public class TodaysGamesActivity extends BaseActivity implements GameSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_todays_games);

        super.onCreate(savedInstanceState);
        Fragment fragment = new TodaysGamesFragment();

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.contentFragment, fragment, GAMES_TAG);
        transaction.commit();
    }

    @Override
    public void onGameSelected(AvailableStats stats) {
        Fragment fragment = new StatsFragment();

        Bundle args = new Bundle();
        args.putSerializable(StatsFragment.STAT_KEY, stats);
        fragment.setArguments(args);

        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.addToBackStack(GAMES_TAG);
        transaction.replace(R.id.contentFragment, fragment, STATS_TAG);
        transaction.commit();
    }
}
