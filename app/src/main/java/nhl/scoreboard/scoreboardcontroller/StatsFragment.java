package nhl.scoreboard.scoreboardcontroller;

import android.app.ListFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.scoreboard.pojo.AvailableStats;
import com.scoreboard.pojo.StatSetting;
import com.scoreboard.pojo.nhl.Game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import nhl.scoreboard.scoreboardcontroller.adapter.StatsAdapter;
import nhl.scoreboard.scoreboardcontroller.cache.StatsCache;
import nhl.scoreboard.scoreboardcontroller.listener.CacheListener;

public class StatsFragment extends ListFragment implements CacheListener {
    public static final String STAT_KEY = "stats";

    private StatsCache statsCache = StatsCache.getInstance();
    private AvailableStats availableStats;
    private StatsAdapter statsAdapter;
    private ArrayList<StatSetting> statSettings = new ArrayList<>();
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle inState) {
        view = inflater.inflate(R.layout.fragment_game_stats, container, false);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            if (getArguments() != null) {
                availableStats = (AvailableStats) getArguments().getSerializable(STAT_KEY);
            }

        } catch (ClassCastException e) {
            throw new RuntimeException("Unexpected value found in arguments: Expected " + STAT_KEY +
                    " to be instance of AvailableStats.class.");
        }

        statsAdapter = new StatsAdapter(getActivity(), statSettings);
        setListAdapter(statsAdapter);

        statsCache.subscribeToUpdateEvents(this);
        updateGameSelection(availableStats);
    }

    public void updateGameSelection(AvailableStats availableStats) {
        this.availableStats = availableStats;
        Game game;

        if (availableStats != null) {
            statSettings.clear();
            statSettings.addAll(availableStats.getStatSettings());
            game = availableStats.getGame();

            TextView textView = (TextView) view.findViewById(R.id.gameTitle);
            textView.setText(game.getTeams().getAwayTeam().getTeam().getAbbreviation() + " @ " +
                    game.getTeams().getHomeTeam().getTeam().getAbbreviation());
            statsAdapter.setGame(game);
        }

        Collections.sort(statSettings, new Comparator<StatSetting>() {
            @Override
            public int compare(StatSetting o1, StatSetting o2) {
                return o1.getStatType().getDisplayValue().compareTo(o2.getStatType().getDisplayValue());
            }
        });

        statsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCacheUpdated() {
        for (AvailableStats availableStats : statsCache.get()) {
            if (this.availableStats != null &&
                    availableStats.getGame().equals(this.availableStats.getGame())) {
                updateGameSelection(availableStats);
            }
        }
    }

    @Override
    public void onDestroyView() {
        statsCache.unsubscribeToUpdateEvents(this);
        super.onDestroyView();
    }
}
