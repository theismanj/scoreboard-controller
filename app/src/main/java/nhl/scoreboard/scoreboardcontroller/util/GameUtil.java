package nhl.scoreboard.scoreboardcontroller.util;

import com.scoreboard.pojo.nhl.Game;

import java.util.ArrayList;
import java.util.List;

import nhl.scoreboard.scoreboardcontroller.pojo.GameInformation;

public class GameUtil {
    public static GameInformation convertGameToGameInformation(Game game) {
        return new GameInformation(game.getId(),
                    game.getTeams().getAwayTeam().getTeam().getAbbreviation(),
                    game.getTeams().getHomeTeam().getTeam().getAbbreviation());
    }
}
