package nhl.scoreboard.scoreboardcontroller.util;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtil {
    private PropertiesUtil() {
    }

    private static Properties properties = null;

    public static Properties initProperties(Context context) {
        if (properties == null) {
            properties = new Properties();

            try {
                AssetManager assetManager = context.getAssets();
                InputStream inputStream = assetManager.open("config.properties");
                properties.load(inputStream);
            } catch (Exception e) {
                System.err.println("Error getting properties file.");
            }
        }

        return properties;
    }

    public static String getServiceURL() {
        checkPropertiesInitialization();
        return properties.getProperty("service.url");
    }

    public static String getAuthorizationHeader() {
        checkPropertiesInitialization();
        return properties.getProperty("auth.header");
    }

    private static void checkPropertiesInitialization() {
        if (properties == null) {
            throw new RuntimeException("Properties file not initialized. Please initialize the properties before " +
                    "attempting to access the properties.");
        }
    }
}
