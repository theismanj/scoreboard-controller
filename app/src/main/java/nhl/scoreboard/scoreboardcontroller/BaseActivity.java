package nhl.scoreboard.scoreboardcontroller;

import android.app.Fragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.scoreboard.pojo.AvailableStats;

import java.util.List;

import nhl.scoreboard.scoreboardcontroller.cache.StatsCache;
import nhl.scoreboard.scoreboardcontroller.dao.StatsDAO;

public class BaseActivity extends AppCompatActivity {
    public static final String GAMES_TAG = "games";
    public static final String STATS_TAG = "stats";

    private int currentOrientation = Configuration.ORIENTATION_UNDEFINED;
    private int layoutResID;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        super.setContentView(layoutResID);

        currentOrientation = getResources().getConfiguration().orientation;

        FloatingActionButton saveFAB = (FloatingActionButton) findViewById(R.id.saveFAB);
        saveFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... params) {
                        StatsDAO.saveStatsSelections();
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        super.onPostExecute(aVoid);
                        Toast.makeText(getBaseContext(), "Stat selection saved.", Toast.LENGTH_LONG).show();
                    }
                }.execute();
            }
        });
    }

    @Override
    public void setContentView(int layoutResID) {
        this.layoutResID = layoutResID;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_todays_games, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            fetchStatsAndCache();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation != currentOrientation) {
            currentOrientation = newConfig.orientation;

            Intent intent;

            if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
                intent = new Intent(this, GamesAndStatsActivity.class);
            } else {
                intent = new Intent(this, TodaysGamesActivity.class);
            }

            startActivity(intent);
        }
    }

    public void fetchStatsAndCache() {
        AsyncTask<String, Void, List<AvailableStats>> asyncTask = new AsyncTask<String, Void, List<AvailableStats>>() {
            @Override
            protected List<AvailableStats> doInBackground(String... params) {
                return StatsDAO.getAvailableStats();
            }

            @Override
            protected void onPostExecute(List<AvailableStats> availableStatsList) {
                super.onPostExecute(availableStatsList);
                StatsCache.getInstance().set(availableStatsList);
            }
        };

        asyncTask.execute();
    }
}
